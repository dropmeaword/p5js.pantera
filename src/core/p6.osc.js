export class OSC {

  constructor( config ) {
    this.config = config;
  }

  receiveOsc(address, value) {
    console.log("received OSC: " + address + ", " + value);
    if (address === '/shape') {
      console.log("setting shape to: " + value[0] );
      console.log("setting color to: " + value[1] );
      shape = value[0];
      col = value[1];
    }
  }
  
  sendOsc(address, value) {
    socket.emit('message', [address, value]);
  }
  
  init(oscPortIn, oscPortOut) {
    socket = io.connect('http://127.0.0.1:8081', { port: 8081, rememberTransport: false });
    socket.on('connect', function() {
      socket.emit('config', {	
        server: { port: oscPortIn,  host: '127.0.0.1'},
        client: { port: oscPortOut, host: '127.0.0.1'}
      });
    });
    socket.on('connect', function() {
      isConnected = true;
    });
    socket.on('message', function(msg) {
      if (msg[0] == '#bundle') {
        for (var i=2; i<msg.length; i++) {
          receiveOsc(msg[i][0], msg[i].splice(1));
        }
      } else {
        receiveOsc(msg[0], msg.splice(1));
      }
    });
  }

}
