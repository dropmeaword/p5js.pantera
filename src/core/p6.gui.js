/** statusbar stuff */
export class Rebar {
  constructor() {
    this.HIDE_KEY = 'h';

    this.template = '<div id="statusbar" class="status">' +
                        '<div class="mcu-status unavailable">&#8277;</div>'+
                        '<div>no hardware detected</div>'+
                        '<div>OSC listening on port 8081</div>'+
                        '<div>MQTT listening on port 8091</div></div>';

    this._visible = true;
  }

  toggle() {
    if(this._visible) {
        hide();
        this._visible = false;
    } else {
        show();
        this._visible = true;
    }
  }

  show() {
    document.getElementById("statusbar").style.visibility="visible";
    this._visible = true;
  }

  hide() {
    document.getElementById("statusbar").style.visibility="hidden";
    this._visible = false;
  }

  handleKeyUp(e) {
    if (e.key === 'h' || e.key === 'H') {
        if(rebar._visible === true) {
            rebar.hide();
        } else {
            rebar.show();
        }
    }
  }

}


export var rebar = new Rebar();
