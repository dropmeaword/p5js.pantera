# Pantera

![pinkpanther](media/pink-panther.jpg)

Pantera is the bootstrapping kit for p5js projects used at the Data+Matter Lab. Pantera includes all the p5js libraries (core, dom and sound) as well as dat.gui and p5.serial. 

The sketch defaults to full window size and resizes the canvas when the parent window is resized. The default sketch draw the PANTERA logo and it demonstrates the use of the animation loop, custome fonts and SVG graphics loading and display. I hope this is a good start to get you rolling.

### Workflow

This project uses the Gulp task-runner, [so make sure you have it installed](https://gulpjs.com/). Gulp is used for the following tasks:
- compile sass from the app/scss folder to the app/styles
- concat any js files in the app/scripts/includes folder in a (non minified) includes.js in the scripts folder along the usual sketch.js file.
- create a local server with browser-sync to serve local the sketch (with live reload).

### Running

To start a project : 
- Clone the repo
- cd into the `p5js.pantera` folder
- run `npm install` (and then wait for a very long time)
- launch with `gulp serve`

### Arduino support
If you need to read data from a serial device like an Arduino you also need to run the serial data pump: `./serialpump.js`


### Frameworks included

Pantera makes use of the following open source librares that can be optionally loaded into your sketches.

- [chroma.js](https://vis4.net/chromajs/) by Gregor Aisch
- [dat.gui](http://workshop.chromeexperiments.com/examples/gui/#1--Basic-Usage) by Data Arts Team, Google Creative Lab
- [firmatajs](https://github.com/firmata/firmata.js) by Julian Gautier and contributors 
- [p5js](https://p5js.org/) by Lauren McCarthy and contributors
- [p5js.serialport](https://github.com/p5-serial/p5.serialport) Shawn Van Every, Jen Kagan, Tom Igoe 
- p5js.sound Jason Sigal and The Processing Foundation
- [p5js.speech](https://idmnyu.github.io/p5.js-speech/) by R. Luke DuBois 
- [js-state-machine](https://github.com/jakesgordon/javascript-state-machine) by Jake Gordon
