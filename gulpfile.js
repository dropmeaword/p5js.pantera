var gulp 		= require('gulp');
var browserSync = require('browser-sync');
var sass 		= require('gulp-sass');
var concat 		= require('gulp-concat');
var run         = require('gulp-run');
var rollup      = require('gulp-rollup');

var reload 		= browserSync.reload;

var src = {
    scss: 'examples/scss/*.scss',
    css:  'examples/css/core',
    build: 'examples/build',
    out: 'pantera.bundle.js',
    build_libs: 'examples/build/libs',
    includes:  'src/core/**/*.js',
    frameworks:  'src/libs/**/*.js',
    html: 'examples/*.html'
};

/* start a webserver on our `app` directory */
gulp.task('serve', ['sass', 'build'], function() {

    browserSync({
        server: "./examples",
        directory: true /* show directory listing if index.html is missing */
        });

    gulp.watch(src.scss, ['sass']);
    gulp.watch(src.includes, ['build']);
    gulp.watch([src.html,src.build]).on('change', reload);
});

/* compile all sass/css files */
gulp.task('sass', function() {
    return gulp.src(src.scss)
        .pipe(sass())
        .pipe(gulp.dest(src.css))
        .pipe(reload({stream: true}));
});

/* copy all framework libraries verbatim */
gulp.task('copy', function() {
    return gulp.src(src.frameworks)
        .pipe(gulp.dest(src.build_libs));
});

/** use rollup.js to boundle our stuff, prefer to call external process so that config file is on disk */
gulp.task('bundle', function() {
    return run('rollup -c rollup.config.js').exec();  
})

/* concatenate all scripts into one single file */
gulp.task('build', ['copy', 'bundle']);  

/* executes the data pump for Serial */
gulp.task('pump-serial', function() {
    var serial = run('node bin/serialpump.js').exec();  
    return serial;
})

/* executes the data pump for OSC */
gulp.task('pump-osc', function() {
    return run('node bin/server/p5.oscbroker.js').exec();  
})

/** run all the pumps that bridge data to the browser */
gulp.task('pumps', ['pump-serial', 'pump-osc']);
/** default to serve mode */
gulp.task('default', ['serve']);
